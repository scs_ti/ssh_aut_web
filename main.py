from paramiko import SSHClient, AutoAddPolicy
import datetime


class ssh:
    def __init__(self):
        self.client = SSHClient()

    def connection(self):
        self.client.set_missing_host_key_policy(AutoAddPolicy())
        try:
            self.client.connect(hostname='10.201.0.120', port=22, username='root', password='#Staa2020')
            print('\n>>> Conectado ao servidor 10.201.0.120 com sucesso')
            ssh.select_option(self)
        except:
            return 'Falha ao conectar'

    def select_option(self):
        value = 0
        hora = str(datetime.datetime.now().time()).split(':')
        if int(hora[0]) >= 22:
            value = 1
        elif int(hora[0]) < 22:
            value = 2
        ssh.command(self, value)

    def command(self, choice):
        if choice == 1:
            try:
                stdin_, stdout_, stderr_ = self.client.exec_command(
                    "cd /opt/payara5/glassfish/domains/domain1/applications/"
                    "autorizacao\n mv solicitar.xhtml solicitarr.xhtml\n ls")
                stdout_.channel.recv_exit_status()
                print('Mudança realizada com Sucesso!')
            except:
                print('Falha ao realizar mudança')

        elif choice == 2:
            try:
                stdin_, stdout_, stderr_ = self.client.exec_command(
                    "cd /opt/payara5/glassfish/domains/domain1/applications/"
                    "autorizacao\n mv solicitarr.xhtml solicitar.xhtml\n ls")
                stdout_.channel.recv_exit_status()
                print('Mudança realizada com Sucesso!')
            except:
                print('Falha ao realizar mudança')


s = ssh()
s.connection()